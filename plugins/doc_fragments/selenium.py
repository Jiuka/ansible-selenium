# -*- coding: utf-8 -*-

# Copyright: (c) 2019, Marius Rieder <marius.rieder@durchmesser.ch>
# MIT License (see licenses/MIT-license.txt or https://opensource.org/licenses/MIT)


class ModuleDocFragment(object):
    DOCUMENTATION = r'''
options:
    executor:
        description:
            - URL of the Selenium server.
            - If not specified, the value of environment variable C(SELENIUM_EXECUTOR) will be used instead.
        type: str
    session:
        description:
            - Session to try to reconnect to.
            - If not specified, the value of environment variable C(SELENIUM_SESSION) will be used instead.
        type: str
    capabilities:
        description:
            - Name of the browser to use as capabilities.
            - If not specified, the value of environment variable C(SELENIUM_BROWSER) will be used instead.
    headless:
        description:
            - If the Browser should be headless.
            - C(FIREFOX) and C(CHROME) only.
        type: bool
        required: false
'''
