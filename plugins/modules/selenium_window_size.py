#!/usr/bin/python

# Copyright: (c) 2019, Marius Rieder <marius.rieder@durchmesser.ch>
# MIT License (see licenses/MIT-license.txt or https://opensource.org/licenses/MIT)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
module: selenium_window_size

short_description: Resize of the browser window.

version_added: "0.0.2"

description:
    - Change the size of the browser window.

options:
    width:
        description: Width of the browser window.
        type: int
    height:
        description: Height of the browser window.
        type: int
    fullscreen:
        description: Invokes the window manager-specific ‘full screen’ operation.
        type: bool
    maximize:
        description: Invokes the window manager-specific ‘maximize’ operation.
        type: bool
    minimize:
        description: Invokes the window manager-specific ‘minimize’ operation.
        type: bool

extends_documentation_fragment:
    - jiuka.selenium.selenium

author:
    - Marius Rieder (@Jiuka)
'''

EXAMPLES = '''
- name: Resize the browser window
  selenium_click:
    session: '{{ selenium.session }}'
    width: 420
    height: 230
'''

RETURN = '''
size:
    description: Browser window size
    type: dict
    returnd: success
    sample:
        width: 800
        height: 600
url:
    description: URL of the browser after the click.
    type: str
    returned: success
title:
    description: Title of the page after the click.
    type: str
    returnd: success
executor:
    description: URL of the Selenium executor used
    type: str
    returned: always
session:
    description: Selenium session id.
    type: str
    returned: always
browser:
    description: Name of the Selenium browser used.
    type: str
    returned: always
'''

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.jiuka.selenium.plugins.module_utils.selenium import AnsibleSeleniumDriver

from itertools import combinations


def main():
    # define available arguments/parameters a user can pass to the module
    module_args = AnsibleSeleniumDriver.argument_spec(dict(
        width=dict(type='int'),
        height=dict(type='int'),
        fullscreen=dict(type='bool'),
        maximize=dict(type='bool'),
        minimize=dict(type='bool'),
    ))

    # seed the result dict in the object
    result = dict(
        changed=False,
    )

    # Setup the AnsibleModule object
    module = AnsibleModule(
        argument_spec=module_args,
        mutually_exclusive=list(combinations(['width', 'fullscreen', 'maximize', 'minimize'], 2)),
        required_together=[['width', 'height']],
    )

    # Setup WebDriver
    driver = AnsibleSeleniumDriver(module)
    result.update(driver.result)

    if module._diff:
        result['diff'] = dict(
            before=driver.get_window_size()
        )

    if module.params['width']:
        driver.set_window_size(module.params['width'], module.params['height'])
    elif module.params['fullscreen']:
        driver.fullscreen_window()
    elif module.params['maximize']:
        driver.maximize_window()
    elif module.params['minimize']:
        driver.minimize_window()

    if module._diff:
        result['diff']['after'] = driver.get_window_size()

    result['changed'] = True
    result['url'] = driver.current_url
    result['title'] = driver.title
    result['size'] = driver.get_window_size()

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


if __name__ == '__main__':
    main()
