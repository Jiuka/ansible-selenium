#!/usr/bin/python

# Copyright: (c) 2019, Marius Rieder <marius.rieder@durchmesser.ch>
# MIT License (see licenses/MIT-license.txt or https://opensource.org/licenses/MIT)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
module: selenium_navigate

short_description: Navigate in a selenium browser.

version_added: "0.0.1"

description:
    - Navigate in a selenium browser.

options:
    get:
        description: URL to load in the browser.
        type: str
        aliases: ['url']
    back:
        description: Go back in the history.
        type: bool
    forward:
        description: Go forward in the history.
        type: bool
    reload:
        description: Reload the current page.
        type: bool
    close:
        description: Cloase the current session.
        type: bool

extends_documentation_fragment:
    - jiuka.selenium.selenium

author:
    - Marius Rieder (@Jiuka)
'''

EXAMPLES = '''
- name: Get Page
  selenium_navigate:
    get: https://www.ansible.com/
  register: selenium

- name: Go back
  selenium_navigate:
    back: true
    session: '{{ selenium.session }}'

- name: Go Forward
  selenium_navigate:
    forward: true
    session: '{{ selenium.session }}'
'''

RETURN = '''
url:
    description: URL of the browser after the click.
    type: str
    returned: success
title:
    description: Title of the page after the click.
    type: str
    returnd: success
executor:
    description: URL of the Selenium executor used
    type: str
    returned: always
session:
    description: Selenium session id.
    type: str
    returned: always
browser:
    description: Name of the Selenium browser used.
    type: str
    returned: always
'''

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.jiuka.selenium.plugins.module_utils.selenium import AnsibleSeleniumDriver


def main():
    # define available arguments/parameters a user can pass to the module
    module_args = AnsibleSeleniumDriver.argument_spec(dict(
        get=dict(type='str',
                 aliases=['url'],
                 required=False),
        back=dict(type='bool',
                  required=False),
        forward=dict(type='bool',
                     required=False),
        reload=dict(type='bool',
                    required=False),
        close=dict(type='bool',
                   required=False),
    ))

    # seed the result dict in the object
    result = dict(
        changed=False,
    )

    # Setup the AnsibleModule object
    module = AnsibleModule(
        argument_spec=module_args,
        mutually_exclusive=[('get', 'back', 'forward', 'reload')],
        required_one_of=[('get', 'back', 'forward', 'reload', 'close')],
    )

    # Setup WebDriver
    driver = AnsibleSeleniumDriver(module)
    result.update(driver.result)

    if module._diff:
        result['diff'] = dict(
            before=dict(
                url=driver.current_url,
                title=driver.title
            ),
        )

    if module.params['get']:
        driver.get(module.params['get'])
    elif module.params['back']:
        driver.back()
    elif module.params['forward']:
        driver.forward()
    elif module.params['reload']:
        driver.reload()

    if module._diff:
        result['diff']['after'] = dict(
            url=driver.current_url,
            title=driver.title
        )

    result['url'] = driver.current_url
    result['title'] = driver.title
    result['changed'] = True

    if module.params['close']:
        driver.close()

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


if __name__ == '__main__':
    main()
