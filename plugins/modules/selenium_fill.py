#!/usr/bin/python

# Copyright: (c) 2019, Marius Rieder <marius.rieder@durchmesser.ch>
# MIT License (see licenses/MIT-license.txt or https://opensource.org/licenses/MIT)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: selenium_fill

short_description: Fill forms in a selenium browser.

version_added: "0.0.1"

description:
    - Fills in forms in a selenium session.

options:
    fill:
        description: List of thinks to fill out.
        type: list
        elements: dict
        required: true
        suboptions:
            class_name:
                description: Finds the element to fill in by class name.
                type: str
            css_selector:
                description: Finds the element to fill in by css selector.
                type: str
            id:
                description: Finds the element to fill in by id.
                type: str
            link_text:
                description: Finds the element to fill in by link_text.
                type: str
            name:
                description: Finds the element to fill in by name.
                type: str
            partial_link_text:
                description: Finds the element to fill in by a partial match of its link text.
                type: str
            tag_name:
                description: Finds the element to fill in by tag name.
                type: str
            xpath:
                description: Finds the element to fill in by xpath.
                type: str
            value:
                description: Value to fill in.
                type: str
                required: true

extends_documentation_fragment:
    - jiuka.selenium.selenium

author:
    - Marius Rieder (@Jiuka)
'''

EXAMPLES = '''
- name: Fill the Search field of the Ansible Documentation
  selenium_fill:
    session: '{{ selenium.session }}'
    fill:
      - xpath: "//input[@name='q']"
        value: Selenium

- name: Fill out the login form
  selenium_fill:
    session: '{{ selenium.session }}'
    fill:
      - id: login_field
        value: alice
      - id: password
        value: bob4ever
'''

RETURN = '''
executor:
    description: URL of the Selenium executor used
    type: str
    returned: always
session:
    description: Selenium session id.
    type: str
    returned: always
browser:
    description: Name of the Selenium browser used.
    type: str
    returned: always
'''

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.jiuka.selenium.plugins.module_utils.selenium import AnsibleSeleniumDriver


def main():
    # define available arguments/parameters a user can pass to the module
    module_args = AnsibleSeleniumDriver.argument_spec(dict(
        fill=dict(type='list',
                  required=True,
                  options=dict(
                      class_name=dict(type='str'),
                      css_selector=dict(type='str'),
                      id=dict(type='str'),
                      link_text=dict(type='str'),
                      name=dict(type='str'),
                      partial_link_text=dict(type='str'),
                      tag_name=dict(type='str'),
                      xpath=dict(type='str'),
                      value=dict(type='str', required=True),
                  )),
    ))

    # seed the result dict in the object
    result = dict(
        changed=False,
    )

    # Setup the AnsibleModule object
    module = AnsibleModule(
        argument_spec=module_args,
    )

    # Setup WebDriver
    driver = AnsibleSeleniumDriver(module)
    result.update(driver.result)

    for fill in module.params['fill']:

        element = None

        if fill.get('class_name', False):
            element = driver.find_element_by_class_name(fill['class_name'])
        elif fill.get('css_selector', False):
            element = driver.find_element_by_css_selector(fill['css_selector'])
        elif fill.get('id', False):
            element = driver.find_element_by_id(fill['id'])
        elif fill.get('link_text', False):
            element = driver.find_element_by_link_text(fill['link_text'])
        elif fill.get('name', False):
            element = driver.find_element_by_name(fill['name'])
        elif fill.get('partial_link_text', False):
            element = driver.find_element_by_partial_link_text(fill['partial_link_text'])
        elif fill.get('tag_name', False):
            element = driver.find_element_by_tag_name(fill['tag_name'])
        elif fill.get('xpath', False):
            element = driver.find_element_by_xpath(fill['xpath'])

        element.send_keys(fill['value'])

    result['changed'] = True

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


if __name__ == '__main__':
    main()
