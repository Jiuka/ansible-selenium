#!/usr/bin/python

# Copyright: (c) 2019, Marius Rieder <marius.rieder@durchmesser.ch>
# MIT License (see licenses/MIT-license.txt or https://opensource.org/licenses/MIT)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
module: selenium_element_info

short_description: Returns info about a element.

version_added: "0.0.2"

description:
    - Returns informations about a element in the selenium browser.

options:
    class_name:
        description: Finds the element to examin by class name.
        type: str
    css_selector:
        description: Finds the element to examin by css selector.
        type: str
    id:
        description: Finds the element to examin by id.
        type: str
    link_text:
        description: Finds the element to examin by link_text.
        type: str
    name:
        description: Finds the element to examin by name.
        type: str
    partial_link_text:
        description: Finds the element to examin by a partial match of its link text.
        type: str
    tag_name:
        description: Finds the element to examin by tag name.
        type: str
    xpath:
        description: Finds the element to examin by xpath.
        type: str
    attributes:
        description: List of attributes to return.
        type: list

extends_documentation_fragment:
    - jiuka.selenium.selenium

author:
    - Marius Rieder (@Jiuka)
'''

EXAMPLES = '''
- name: Get info about a element
  selenium_element:info:
    session: '{{ selenium.session }}'
    tag_name: a
'''

RETURN = '''
attributes:
    description: Hash of attributes of the element according to the attributes option.
    type: dict
    returned: if attributes option is given.
name:
    description: Tag name of the element clicked.
    type: str
    returned: success
text
    description: Text content of the element clicked.
    type: str
    returned: success
url:
    description: URL of the browser after the click.
    type: str
    returned: success
title:
    description: Title of the page after the click.
    type: str
    returnd: success
executor:
    description: URL of the Selenium executor used
    type: str
    returned: always
session:
    description: Selenium session id.
    type: str
    returned: always
browser:
    description: Name of the Selenium browser used.
    type: str
    returned: always
'''

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.jiuka.selenium.plugins.module_utils.selenium import AnsibleSeleniumDriver


def main():
    # define available arguments/parameters a user can pass to the module
    module_args = AnsibleSeleniumDriver.argument_spec(dict(
        class_name=dict(type='str'),
        css_selector=dict(type='str'),
        id=dict(type='str'),
        link_text=dict(type='str'),
        name=dict(type='str'),
        partial_link_text=dict(type='str'),
        tag_name=dict(type='str'),
        xpath=dict(type='str'),
        attributes=dict(type='list', aliases=['attribute']),
    ))

    # seed the result dict in the object
    result = dict(
        changed=False,
    )

    # Setup the AnsibleModule object
    module = AnsibleModule(
        argument_spec=module_args,
    )

    # Setup WebDriver
    driver = AnsibleSeleniumDriver(module)
    result.update(driver.result)

    element = None

    if module.params['class_name']:
        element = driver.find_element_by_class_name(module.params['class_name'])
    elif module.params['css_selector']:
        element = driver.find_element_by_css_selector(module.params['css_selector'])
    elif module.params['id']:
        element = driver.find_element_by_id(module.params['id'])
    elif module.params['link_text']:
        element = driver.find_element_by_link_text(module.params['link_text'])
    elif module.params['name']:
        element = driver.find_element_by_name(module.params['name'])
    elif module.params['partial_link_text']:
        element = driver.find_element_by_partial_link_text(module.params['partial_link_text'])
    elif module.params['tag_name']:
        element = driver.find_element_by_tag_name(module.params['tag_name'])
    elif module.params['xpath']:
        element = driver.find_element_by_xpath(module.params['xpath'])

    result['name'] = element.tag_name
    result['text'] = element.text
    result['url'] = driver.current_url
    result['title'] = driver.title

    if module.params['attributes']:
        result['attributes'] = {}

    for attribute in module.params['attributes']:
        result['attributes'][attribute] = element.get_attribute(attribute)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


if __name__ == '__main__':
    main()
