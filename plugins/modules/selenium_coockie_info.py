#!/usr/bin/python

# Copyright: (c) 2019, Marius Rieder <marius.rieder@durchmesser.ch>
# MIT License (see licenses/MIT-license.txt or https://opensource.org/licenses/MIT)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
module: selenium_cookie_info

short_description: Fetches cookies from a selenium browser.

version_added: "0.0.1"

description:
    - Returns the cookies, optionaly matching the name and domain, of a selenium browser session.

options:
    name:
        description:
            - Only return cookies matching this name.
        type: str
        required: false
    domain:
        description:
            - Only return cookies beloning to the domain or a subdomain.
        type: str
        required: false

extends_documentation_fragment:
    - jiuka.selenium.selenium

author:
    - Marius Rieder (@Jiuka)
'''

EXAMPLES = '''
'''

RETURN = '''
cookies
    description: List of cookies of the selenium browser.
    type: list
    returned: success
url:
    description: URL of the browser after the click.
    type: str
    returned: success
title:
    description: Title of the page after the click.
    type: str
    returnd: success
executor:
    description: URL of the Selenium executor used
    type: str
    returned: always
session:
    description: Selenium session id.
    type: str
    returned: always
browser:
    description: Name of the Selenium browser used.
    type: str
    returned: always
'''

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.jiuka.selenium.plugins.module_utils.selenium import AnsibleSeleniumDriver


def main():
    # define available arguments/parameters a user can pass to the module
    module_args = AnsibleSeleniumDriver.argument_spec(dict(
        name=dict(type='str',
                  required=False),
        domain=dict(type='str',
                    required=False),
    ))

    # seed the result dict in the object
    result = dict(
        changed=False,
    )

    # Setup the AnsibleModule object
    module = AnsibleModule(
        argument_spec=module_args,
    )

    # Setup WebDriver
    driver = AnsibleSeleniumDriver(module)
    result.update(driver.result)

    cookies = driver.get_cookies()

    if module.params['name']:
        cookies = list(filter(lambda c: c['name'] == module.params['name'], cookies))

    if module.params['domain']:
        cookies = list(filter(lambda c: c['domain'].endswith(module.params['domain']), cookies))

    result['url'] = driver.current_url
    result['title'] = driver.title
    result['cookies'] = cookies
    result['changed'] = False

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


if __name__ == '__main__':
    main()
