#!/usr/bin/python

# Copyright: (c) 2019, Marius Rieder <marius.rieder@scs.ch>
# MIT License (see licenses/MIT-license.txt or https://opensource.org/licenses/MIT)

from ansible.module_utils.basic import env_fallback

from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class AnsibleSeleniumDriver(object):
    BROWSERS = ['FIREFOX', 'INTERNETEXPLORER', 'EDGE', 'CHROME', 'OPERA',
                'SAFARI', 'HTMLUNIT', 'HTMLUNITWITHJS', 'IPHONE', 'IPAD',
                'ANDROID', 'PHANTOMJS', 'WEBKITGTK']

    def __init__(self, module):
        self._module = module
        self.result = dict()

        self._open()

    @classmethod
    def argument_spec(cls, spec=None):
        arg_spec = dict(
            executor=dict(type='str',
                          required=False,
                          aliases=['hub'],
                          default='http://127.0.0.1:4444/wd/hub',
                          fallback=(env_fallback, ['SELENIUM_EXECUTOR'])),
            session=dict(type='str',
                         required=False,
                         fallback=(env_fallback, ['SELENIUM_SESSION'])),
            capabilities=dict(type='str',
                              required=False,
                              aliases=['browser'],
                              default='HTMLUNITWITHJS',
                              choices=cls.BROWSERS,
                              fallback=(env_fallback, ['SELENIUM_BROWSER'])),
            headless=dict(type='bool'),
        )
        if spec:
            arg_spec.update(spec)
        return arg_spec

    def __getattr__(self, name):
        return getattr(self._driver, name)

    def _open(self):
        capabilities = getattr(DesiredCapabilities, self._module.params['capabilities'])

        options = None
        if self._module.params['capabilities'] == 'FIREFOX' and self._module.params['headless'] is not None:
            options = webdriver.firefox.options.Options()
            options.set_headless(self._module.params['headless'])
        elif self._module.params['capabilities'] == 'CHROME' and self._module.params['headless'] is not None:
            options = webdriver.chrome.options.Options()
            options.set_headless(self._module.params['headless'])

        try:
            self._driver = webdriver.Remote(command_executor=self._module.params['executor'],
                                            desired_capabilities=capabilities,
                                            options=options)
        except Exception as e:
            self._module.fail_json(msg='Error opening WebDriver: {}'.format(e))

        if self._module.params['session']:
            self._driver.close()
            self._driver.session_id = self._module.params['session']

        self.result['executor'] = self._module.params['executor']
        self.result['session'] = self._driver.session_id
        self.result['browser'] = self._driver.name
