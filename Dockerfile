FROM python:alpine

RUN mkdir -p /ansible_collections/jiuka/selenium

RUN apk add --quiet --no-cache --virtual .pip \
        gcc \
        libc-dev \
        openssl-dev \
        libffi-dev && \
    pip install --quiet ansible selenium && \
    apk del --no-network .pip && \
    rm -rf /root/.cache

ENV ANSIBLE_COLLECTIONS_PATHS=/
WORKDIR /ansible_collections/jiuka/selenium
CMD /bin/sh
