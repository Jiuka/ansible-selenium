# Selenium Ansible Collection

A collection of Ansible modules that can drive a Browser with selenium.

## Modules

### selenium_click

Click on a given element.

### selenium_fill

Fill in fomular.

### selenium_navigate

Navigate to new pages or back and forth in the history.

## Info Modules

### selenium_coockie_info

Get cookies from the session.

### selenium_screenshot

Take a screenshot of the whole browser or a specific element.

